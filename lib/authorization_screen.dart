import 'package:flutter/material.dart';
import "./custom_carousel.dart";
import './area_of_text.dart';

class AuthorizationScreen extends StatefulWidget {
  const AuthorizationScreen({super.key, required this.subList});
  final Map<String, dynamic> subList;
  @override
  State<AuthorizationScreen> createState() => _AuthorizationScreenState();
}

class _AuthorizationScreenState extends State<AuthorizationScreen> {
  bool _flag = true;
  void awakeIt() {
    // print("!!!!!!!!!!!!!!!!! I'M TRYING !!!!!!!!!!!!!!!!!!!");
    setState(() {
      // sleep(Duration(seconds: 5));
      _flag = !_flag;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 55,
          backgroundColor: const Color(0xffE9F6E5),
          elevation: 0,
          centerTitle: true,
          title: Image.asset(
            'assets/logo.png',
            height: 32,
          ),
        ),
        body: GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: Column(
            children: [
              Flexible(
                flex: _flag ? 6 : 2,
                child: CustomCarousel(
                  flag: _flag,
                  sliderList: widget.subList["2443"],
                ),
              ),
              Flexible(
                flex: _flag ? 6 : 4,
                child: AreaOfText(
                    areaOfTextList: widget.subList,
                    hideImage: (bool hasFocus) {
                      awakeIt();
                    }),
              ),
            ],
          ),
        ));
  }
}
