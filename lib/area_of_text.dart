import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:test_case_jun_mamonova/extentions/hex_color_extention.dart';
import 'package:flutter/cupertino.dart';
import 'custom_widgets/custom_text_widget.dart';
import 'custom_widgets/custom_text_field_widget.dart';
import 'custom_widgets/custom_button_widget.dart';
import 'parser.dart';

class AreaOfText extends StatefulWidget {
  const AreaOfText(
      {super.key, required this.hideImage, required this.areaOfTextList});
  final Map<String, dynamic> areaOfTextList;
  final Function(bool) hideImage;

  @override
  State<AreaOfText> createState() => _AreaOfTextState();
}

class _AreaOfTextState extends State<AreaOfText> {
  final FocusNode _focus = FocusNode();
  final formKey = GlobalKey<FormState>();
  // final TextEditingController controller = TextEditingController();

  void _onFocusChange() {
    // print("!!!!" + MediaQuery.of(context).viewInsets.bottom.toString());
    // print("Focus change!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    widget.hideImage(_focus.hasFocus);
  }

  @override
  void initState() {
    super.initState();
    _focus.addListener(_onFocusChange);
    // print(widget.areaOfTextList["2443"]);
  }

  Map<String, dynamic> jsonScreen2 = jsonDecode('''{
    "name": "otpCode",
    "screen": {
      "id": 3033,
      "fields": [
        {
          "id": 76345,
          "fieldType": "text",
          "properties": {
            "visibility": true,
            "textAlign": "center",
            "text": "Enter the 4 digit OTP"
          }
        },
        {
          "id": 535,
          "fieldType": "inputOTP",
          "properties": {
            "visibility": true,
            "numberSymbols": 4,
            "pointColor": "#C4C4C4",
            "borderColor": "#8E8E93"
          }
        },
        {
          "id": 3414,
          "fieldType": "timer",
          "properties": {
            "visibility": true,
            "textAlign": "center",
            "loaderColor": "#B51ACE",
            "timerDuration": 30,
            "waitingText": "Request a new code in {timer}",
            "endText": "Resend a new code",
            "url": ""
          }
        },
        {
          "id": 63245,
          "fieldType": "image",
          "properties": {
            "visibility": true,
            "imageUrl": "https://storage.googleapis.com/cms-storage-bucket/ec64036b4eacc9f3fd73.svg"
          }
        }
      ]
    }
  }''');

  Parser parser = Parser();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomTextWidget(
          listik: widget.areaOfTextList["1122"],
          styles: const TextStyle(
              color: Color(0xff202230),
              fontFamily: "Inter",
              fontWeight: FontWeight.w600,
              fontSize: 22),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: CustomTextWidget(
            listik: widget.areaOfTextList["1123"],
            styles: const TextStyle(
                color: Color(0xff707070),
                fontFamily: "Inter",
                fontWeight: FontWeight.w400,
                fontSize: 17),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 16.0, right: 16, left: 16),
          child: Form(
              key: formKey,
              child: CustomTextFieldWidget(
                  listik: widget.areaOfTextList['1223'], focus: _focus)),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: Align(
              alignment: Alignment.centerLeft,
              child: CustomTextWidget(
                listik: widget.areaOfTextList['1124'],
                styles: const TextStyle(
                  fontFamily: "Inter",
                  fontWeight: FontWeight.w400,
                  fontSize: 12,
                  color: Color(0xff707070),
                ),
              )),
        ),
        !_focus.hasFocus
            ? Padding(
                padding: const EdgeInsets.all(16.0),
                child: CustomTextWidget(
                    listik: widget.areaOfTextList['1125'],
                    styles: const TextStyle(
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        color: Color(0xff707070))),
              )
            : Container(),
        _focus.hasFocus
            ? Padding(
                padding: const EdgeInsets.all(16.0),
                child: SizedBox(
                    height: 53,
                    child: CustomButtonWidget(
                      listik: widget.areaOfTextList["6577"],
                      stylesText: TextStyle(
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        color: HexColor.fromHex(
                            widget.areaOfTextList["6577"]["textColor"]),
                      ),
                      stylesButton: ElevatedButton.styleFrom(
                          side: BorderSide(
                              width: 1,
                              color: HexColor.fromHex(
                                  widget.areaOfTextList["6577"]["borderColor"],
                                  rightColor: const Color(0xffB51ACE))),
                          backgroundColor: HexColor.fromHex(
                              widget.areaOfTextList["6577"]["backgroundColor"],
                              rightColor: const Color(0xffB51ACE)),
                          minimumSize: const Size(double.infinity, 50),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(62),
                          )),
                      onPressFunc: () {
                        if (formKey.currentState!.validate()) {
                          Navigator.push(
                              context,
                              CupertinoPageRoute(
                                  builder: (context) => (parser.buildWidget(
                                      parser.configParser(jsonScreen2)))));
                        }
                      },
                    )),
              )
            : Container(),
      ],
    );
  }
}
