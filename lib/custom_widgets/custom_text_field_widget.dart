import 'package:flutter/material.dart';
import 'package:flutter_masked_text2/flutter_masked_text2.dart';
import 'package:test_case_jun_mamonova/extentions/hex_color_extention.dart';

class CustomTextFieldWidget extends StatelessWidget {
  const CustomTextFieldWidget(
      {super.key, required this.listik, required this.focus});
  final Map<String, dynamic> listik;
  final FocusNode focus;

  @override
  Widget build(BuildContext context) {
    final maskController =
        MaskedTextController(mask: listik["mask"].replaceAll("_", "0"));
    if (listik["visibility"]) {
      return TextFormField(
        focusNode: focus,
        obscureText: listik["security"],
        obscuringCharacter: listik["replacementSymbol"],
        validator: (value) {
          if (maskController.text.length < 18) {
            return listik['errorText'];
          }
        },
        style: const TextStyle(
            fontFamily: "Inter",
            fontWeight: FontWeight.w400,
            fontSize: 17,
            color: Color(0xff242424)),
        decoration: InputDecoration(
          filled: true,
          fillColor: HexColor.fromHex(listik['backgroundColor'],
              rightColor: Color(0xffF5F6FA)),
          hintText: listik["placeholder"],
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
                width: 1,
                color: HexColor.fromHex(listik['borderColor'],
                    rightColor: const Color(0xffF5F6FA))),
            borderRadius: const BorderRadius.all(Radius.circular(16)),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
                width: 1,
                color: HexColor.fromHex(listik['errorBorderColor'],
                    rightColor: Colors.red)),
            borderRadius: const BorderRadius.all(Radius.circular(16)),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
                width: 1,
                color: HexColor.fromHex(listik['borderColor'],
                    rightColor: const Color(0xffF5F6FA))),
            borderRadius: const BorderRadius.all(Radius.circular(16)),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(
                width: 1,
                color: HexColor.fromHex(listik['borderColor'],
                    rightColor: const Color(0xffF5F6FA))),
            borderRadius: const BorderRadius.all(Radius.circular(16)),
          ),
        ),
        controller: maskController,
        keyboardType: TextInputType.number,
      );
    } else {
      return Container();
    }
  }
}
