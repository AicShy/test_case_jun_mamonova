import 'package:flutter/material.dart';

class CustomTextWidget extends StatelessWidget {
  CustomTextWidget({super.key, required this.listik, required this.styles});
  final Map<String, dynamic> listik;
  final TextStyle styles;
  TextAlign align = TextAlign.center;

  Widget buildWidgetText() {
    if (listik['visibility']) {
      switch (listik['textAlign']) {
        case "center":
          align = TextAlign.center;
          break;
        case "left":
          align = TextAlign.left;
          break;
        case "right":
          align = TextAlign.right;
          break;
        case "end":
          align = TextAlign.end;
          break;
        case "start":
          align = TextAlign.start;
          break;
        case "justify":
          align = TextAlign.justify;
          break;
        default:
          align = TextAlign.center;
          break;
      }
      return Text(
        listik["text"],
        textAlign: align,
        style: styles,
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return buildWidgetText();
  }
}
