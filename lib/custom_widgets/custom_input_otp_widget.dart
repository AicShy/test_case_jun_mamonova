import 'package:pinput/pinput.dart';
import 'package:flutter/material.dart';
import 'package:test_case_jun_mamonova/extentions/hex_color_extention.dart';

class CustomInputOTPWidget extends StatelessWidget {
  CustomInputOTPWidget(
      {super.key,
      required this.listik,
      required this.focus,
      required this.controller,
      required this.onFocusChange,
      required this.valueOfField,
      required this.changeValue});
  final Map<String, dynamic> listik;
  final FocusNode focus;
  final TextEditingController controller;
  final VoidCallback onFocusChange;
  String valueOfField;
  Function(String) changeValue;

  @override
  Widget build(BuildContext context) {
    // print(listik);
    if (listik['visibility']) {
      return Pinput(
        length: listik['numberSymbols'],
        focusNode: focus,
        defaultPinTheme: PinTheme(
          width: 56,
          height: 72,
          textStyle: const TextStyle(
              color: Color(0xffA0A0A0),
              fontFamily: "Inter",
              fontWeight: FontWeight.w500,
              fontSize: 56),
          decoration: BoxDecoration(
            border: Border.all(
                color: HexColor.fromHex(listik["borderColor"],
                    rightColor: Colors.transparent)),
            // borderRadius: BorderRadius.circular(20),
          ),
        ),
        focusedPinTheme: PinTheme(
          width: 56,
          height: 72,
          textStyle: const TextStyle(
              color: Color(0xffA0A0A0),
              fontFamily: "Inter",
              fontWeight: FontWeight.w500,
              fontSize: 56),
          decoration: BoxDecoration(
            border: Border.all(
                color: HexColor.fromHex(listik["pointColor"],
                    rightColor: Colors.transparent)),
            // borderRadius: BorderRadius.circular(20),
          ),
        ),
        controller: controller,
        showCursor: true,
        onCompleted: (pin) {
          onFocusChange();
        },
        onChanged: (pin) {
          if (pin.length == 0) {
            // print("сменили на нули");
            valueOfField = '0000';
          } else {
            // print("сменили на пин");
            valueOfField = pin;
          }
          changeValue(valueOfField);
        },
      );
    } else {
      return Container();
    }
  }
}
