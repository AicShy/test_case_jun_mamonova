import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:test_case_jun_mamonova/extentions/hex_color_extention.dart';

import '../dots_indicator.dart';

class CustomSliderWidget extends StatelessWidget {
  CustomSliderWidget(
      {super.key,
      required this.listik,
      required this.flag,
      required this.changeDot,
      required this.titleStyles,
      required this.subtitelStyles,
      required this.currentIndex});

  final Map<String, dynamic> listik;
  bool flag;
  final Function(int) changeDot;
  final TextStyle titleStyles;
  final TextStyle subtitelStyles;
  TextAlign align = TextAlign.center;
  final int currentIndex;

  @override
  Widget build(BuildContext context) {
    if (listik['visibility']) {
      return Column(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(40),
                  bottomRight: Radius.circular(40)),
              color: HexColor.fromHex(listik['backgroundColor'],
                  rightColor: const Color(0xffF2E8FA)),
            ),
            child: CarouselSlider(
                options: CarouselOptions(
                  viewportFraction: 1,
                  aspectRatio: flag ? 18 / 14 : 18 / 5,
                  autoPlay: true,
                  onPageChanged: (index, reason) {
                    // print("Индекс: $index");
                    changeDot(index);
                  },
                ),
                items: (listik["cards"] as List<dynamic>)
                    .map((i) => Builder(
                          builder: (BuildContext context) {
                            return Column(children: [
                              Expanded(
                                child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(
                                              i['title'],
                                              textAlign: TextAlign.center,
                                              style: titleStyles,
                                            ),
                                          ),
                                          SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2,
                                            child: Text(
                                              i['subTitle'],
                                              textAlign: TextAlign.center,
                                              style: subtitelStyles,
                                            ),
                                          ),
                                        ],
                                      ),
                                      flag
                                          ? Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                  Image.network(i['imageUrl'],
                                                      fit: BoxFit.fitHeight,
                                                      loadingBuilder: (context,
                                                          child,
                                                          loadingProgress) {
                                                    if (loadingProgress !=
                                                            null &&
                                                        loadingProgress
                                                                .cumulativeBytesLoaded !=
                                                            loadingProgress
                                                                .expectedTotalBytes) {
                                                      return const CircularProgressIndicator();
                                                    } else {
                                                      return child;
                                                    }
                                                  }),
                                                  const SizedBox(
                                                    height: 30,
                                                  )
                                                ])
                                          : Container(),
                                      // ),
                                    ]),
                              )
                            ]);
                          },
                        ))
                    .toList()),
          ),
          DotsIndicator(
            lenght: listik["cards"].length,
            index: currentIndex,
            colorCircle: listik["colorСircle"],
            activeColorCircle: listik["activeColorСircle"],
          )
        ],
      );
    } else {
      return Container();
    }
    // return buildWidget();
  }
}
