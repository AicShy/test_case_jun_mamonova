import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomSvgImageWidget extends StatelessWidget {
  const CustomSvgImageWidget({super.key, required this.listik});
  final Map<String, dynamic> listik;

  @override
  Widget build(BuildContext context) {
    // print(listik);
    if (listik['visibility']) {
      return SvgPicture.network(
        listik["imageUrl"],
        alignment: Alignment.center,
        color: Colors.purple,
      );
    } else {
      return Container();
    }
  }
}
