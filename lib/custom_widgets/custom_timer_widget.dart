import 'package:flutter/material.dart';
import 'package:test_case_jun_mamonova/extentions/hex_color_extention.dart';

class CustomTimerWidget extends StatefulWidget {
  CustomTimerWidget(
      {super.key,
      required this.listik,
      required this.flag,
      required this.start,
      required this.startTimer,
      required this.setTimer});
  final Map<String, dynamic> listik;
  bool flag;
  int start;
  final VoidCallback startTimer;
  final Function(bool) setTimer;

  @override
  State<CustomTimerWidget> createState() => _CustomTimerWidgetState();
}

class _CustomTimerWidgetState extends State<CustomTimerWidget> {
  String clear(String str) {
    int index = str.indexOf("{");
    return str.substring(0, index);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.listik['visibility']) {
      if (widget.flag) {
        return Column(
          children: [
            Text(clear(widget.listik["waitingText"]),
                textAlign: TextAlign.center,
                style: const TextStyle(
                    color: Color(0xff707070),
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w400,
                    fontSize: 15)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: SizedBox(
                    height: 16,
                    width: 16,
                    child: CircularProgressIndicator(
                      strokeWidth: 2,
                      color: HexColor.fromHex(widget.listik["loadedColor"],
                          rightColor: const Color(0x80B51ACE)),
                      backgroundColor: const Color(0xffF2E8FA),
                    ),
                  ),
                ),
                Text(
                  "00:${widget.start < 10 ? '0' + widget.start.toString() : widget.start}",
                  style: TextStyle(
                    color: HexColor.fromHex(widget.listik["loadedColor"],
                        rightColor: const Color(0x80B51ACE)),
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w400,
                    fontSize: 15,
                  ),
                )
              ],
            ),
          ],
        );
      } else {
        return SizedBox(
          height: 20,
          child: TextButton(
              style: TextButton.styleFrom(
                padding: EdgeInsets.zero,
              ),
              onPressed: () {
                setState(() {
                  widget.flag = true;
                  widget.startTimer();
                });
                widget.setTimer(widget.flag);
              },
              child: Text(
                widget.listik["endText"],
                style: const TextStyle(
                    color: Color(0xff3151C3),
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w700,
                    fontSize: 15),
              )),
        );
      }
    } else {
      return Container();
    }
  }
}
