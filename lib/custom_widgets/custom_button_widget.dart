import 'package:flutter/material.dart';

class CustomButtonWidget extends StatelessWidget {
  CustomButtonWidget(
      {super.key,
      required this.listik,
      required this.stylesText,
      required this.stylesButton,
      required this.onPressFunc});
  final Map<String, dynamic> listik;
  final ButtonStyle stylesButton;
  final TextStyle stylesText;
  final VoidCallback onPressFunc;
  TextAlign align = TextAlign.center;

  Widget buildButton() {
    if (listik['visibility']) {
      switch (listik['textAlign']) {
        case "center":
          align = TextAlign.center;
          break;
        case "left":
          align = TextAlign.left;
          break;
        case "right":
          align = TextAlign.right;
          break;
        case "end":
          align = TextAlign.end;
          break;
        case "start":
          align = TextAlign.start;
          break;
        case "justify":
          align = TextAlign.justify;
          break;
        default:
          align = TextAlign.center;
          break;
      }
      return ElevatedButton(
          style: stylesButton,
          onPressed: onPressFunc,
          child: Text(
            listik["text"],
            style: stylesText,
            textAlign: align,
          ));
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return buildButton();
  }
}
