abstract class SimpleWidget {}

class MainNodeWidget extends SimpleWidget {
  final String name;
  final SimpleWidget screen;
  MainNodeWidget({required this.name, required this.screen});
}

class ScreenWidget extends SimpleWidget {
  final int id;
  final List<SimpleWidget> fields;
  ScreenWidget({required this.id, required this.fields});
}

class TextWidget extends SimpleWidget {
  final int id;
  final bool visibility;
  final String textAlign;
  final String text;
  TextWidget(
      {required this.id,
      required this.visibility,
      required this.textAlign,
      required this.text});
}

class TextFieldWidget extends SimpleWidget {
  final int id;
  final bool visibility;
  final bool security;
  final String backgroundColor;
  final String borderColor;
  final String errorBorderColor;
  final String mask;
  final String replacementSymbol;
  final String label;
  final String text;
  final String placeholder;
  final String errorText;

  TextFieldWidget({
    required this.id,
    required this.visibility,
    required this.security,
    required this.backgroundColor,
    required this.borderColor,
    required this.errorBorderColor,
    required this.mask,
    required this.replacementSymbol,
    required this.label,
    required this.text,
    required this.placeholder,
    required this.errorText,
  });
}

class CardWidget extends SimpleWidget {
  final String title;
  final String subTitle;
  final String imageUrl;
  CardWidget(
      {required this.title, required this.subTitle, required this.imageUrl});
}

class SliderWidget extends SimpleWidget {
  final int id;

  final bool visibility;
  final String backgroundColor;
  final String colorCircle;
  final String activeColorCircle;
  final List<dynamic> cards;

  List<CardWidget> get cardList {
    List<CardWidget> listOfCards = [];
    for (var element in cards) {
      listOfCards.add(CardWidget(
          title: element["title"] ?? "",
          subTitle: element['subTitle'] ?? "",
          imageUrl: element['imageUrl'] ?? ""));
    }
    return listOfCards;
  }

  SliderWidget({
    required this.id,
    required this.visibility,
    required this.backgroundColor,
    required this.colorCircle,
    required this.activeColorCircle,
    required this.cards,
  });
}

class ButtonWidget extends SimpleWidget {
  final int id;
  final bool visibility;
  final String textAlign;
  final String backgroundColor;
  final String textColor;
  final String borderColor;
  final String text;

  ButtonWidget({
    required this.id,
    required this.visibility,
    required this.textAlign,
    required this.backgroundColor,
    required this.textColor,
    required this.borderColor,
    required this.text,
  });
}

class InputOTPWidget extends SimpleWidget {
  final int id;
  final bool visibility;
  final int numberSymbols;
  final String pointColor;
  final String borderColor;

  InputOTPWidget({
    required this.id,
    required this.visibility,
    required this.numberSymbols,
    required this.pointColor,
    required this.borderColor,
  });
}

class TimerWidget extends SimpleWidget {
  final int id;

  final bool visibility;
  final String textAlign;
  final String loaderColor;
  final int timerDuration;
  final String waitingText;
  final String endText;
  final String url;

  TimerWidget({
    required this.id,
    required this.visibility,
    required this.textAlign,
    required this.loaderColor,
    required this.timerDuration,
    required this.waitingText,
    required this.endText,
    required this.url,
  });
}

class ImageWidget extends SimpleWidget {
  final int id;
  final bool visibility;
  final String imageUrl;
  ImageWidget(
      {required this.id, required this.visibility, required this.imageUrl});
}

class ErrorCustomWidget extends SimpleWidget {
  final String errorText;
  ErrorCustomWidget({this.errorText = "Error"});
}
