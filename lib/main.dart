import 'dart:convert';

import 'package:flutter/material.dart';
import 'parser.dart';

Map<String, dynamic> jsonScreen1 = jsonDecode('''{
    "name": "authotization",
    "screen": {
      "id": 214,
      "fields": [
        {
          "id": 1122,
          "fieldType": "text",
          "properties": {
            "visibility": true,
            "textAlign": "center",
            "text": "Enter your phone number"
          }
        },
        {
          "id": 1123,
          "fieldType": "text",
          "properties": {
            "visibility": true,
            "textAlign": "center",
            "text": "We will send you a verification number"
          }
        },
        {
          "id": 1223,
          "fieldType": "textField",
          "properties": {
            "visibility": true,
            "security": false,
            "backgroundColor": "#F5F7FB",
            "borderColor": "",
            "errorBorderColor": "#E32A5F",
            "mask": "+91 ___ ____ __ __",
            "replacementSymbol": "_",
            "label": "Phone number:",
            "text": "",
            "placeholder": "+91",
            "errorText": "Please enter your phone number to proceed"
          }
        },
        {
          "id": 1124,
          "fieldType": "text",
          "properties": {
            "visibility": true,
            "textAlign": "left",
            "text": "Your mobile number should be linked with your Aadhaar"
          }
        },
        {
          "id": 1125,
          "fieldType": "text",
          "properties": {
            "visibility": true,
            "textAlign": "left",
            "text": "By signing up I give my consent to VIVA MONEY for authentication through OTP and verify my OTP for availing the loan. I agree to the Terms and Conditions and Privacy Policy"
          }
        },
        {
          "id": 2443,
          "fieldType": "slider",
          "properties": {
            "backgroundColor": "#E9F6E5",
            "colorСircle": "#E9ECF8",
            "activeColorСircle": "#B51ACE",
            "cards": [
              {
                "title": "Grace period up to 51 days",
                "subTitle": "free, without hidden feesand overpayments",
                "imageUrl": "https://www.google.com/logos/doodles/2023/kitty-oneils-77th-birthday-6753651837110033.3-2x.png"
              },
              {
                "title": "",
                "subTitle": "",
                "imageUrl": "https://www.google.com/logos/doodles/2022/thanksgiving-2022-6753651837109542.5-2x.png"
              },
              {
                "title": "Grace period up to 51 days",
                "subTitle": "",
                "imageUrl": "https://www.google.com/logos/doodles/2023/st-patricks-day-2023-6753651837109680-2x.png"
              }
            ],
            "visibility": true
          }
        },
        {
          "id": 6577,
          "fieldType": "button",
          "properties": {
            "textAlign": "center",
            "visibility": true,
            "backgroundColor": "",
            "textColor": "#287018",
            "borderColor": "",
            "text": "Next"
          }
        }
      ]
    }
  }''');

void main() {
  Parser parser = Parser();

  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      // home: AutorizationScreen(),
      // home: OTPInputPage(),
      home: parser.buildWidget(parser.configParser(jsonScreen1)),
    ),
  );
  // print((jsonScreen1["screen"]["fields"] as List<dynamic>).length);
  // SimpleWidget widget = parser.configParser(jsonScreen1);
  // List<dynamic> name = parser.addProperties(widget);
  // print(widget);
  // print(parser.configParser(jsonScreen1));
  // print(jsonScreen1['screen']);
  // print(widget.toString());
  // print("final ${parser.addProperties(widget)}");
}
