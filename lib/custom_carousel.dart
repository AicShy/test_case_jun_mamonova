import 'package:flutter/material.dart';
import 'package:test_case_jun_mamonova/custom_widgets/custom_slider_widget.dart';

class CustomCarousel extends StatefulWidget {
  const CustomCarousel(
      {super.key, required this.flag, required this.sliderList});
  final bool flag;
  final Map<String, dynamic> sliderList;

  @override
  State<CustomCarousel> createState() => _CustomCarouselState();
}

class _CustomCarouselState extends State<CustomCarousel> {
  int currentIndex = 0;

  void changeDot(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CustomSliderWidget(
      listik: widget.sliderList,
      flag: widget.flag,
      changeDot: changeDot,
      titleStyles: const TextStyle(
          color: Color(0xff0E1A22),
          fontFamily: "Inter",
          fontWeight: FontWeight.w600,
          fontSize: 20),
      subtitelStyles: const TextStyle(
          color: Color(0xff0E1A22),
          fontFamily: "Inter",
          fontWeight: FontWeight.w400,
          fontSize: 15),
      currentIndex: currentIndex,
    );
  }
}
