import 'package:flutter/material.dart';
import 'package:test_case_jun_mamonova/extentions/hex_color_extention.dart';

class DotsIndicator extends StatelessWidget {
  const DotsIndicator({
    super.key,
    required this.lenght,
    required this.index,
    required this.colorCircle,
    required this.activeColorCircle,
  });
  final int lenght;
  final int index;
  final String colorCircle;
  final String activeColorCircle;

  List<Widget> genDots(int lenght, int index) {
    // print("Индекс в дотсах $index");
    List<Widget>? dots = [];
    for (int i = 0; i < lenght; i++) {
      if (i != index) {
        dots.add(Container(
            width: 8,
            height: 8,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: HexColor.fromHex(colorCircle,
                    rightColor: const Color(0xffD9D9D9)))));
      } else {
        dots.add(Container(
            width: 8,
            height: 8,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: HexColor.fromHex(activeColorCircle,
                    rightColor: const Color(0xffB51ACE)))));
      }
    }
    return dots;
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        constraints: const BoxConstraints(maxWidth: 72),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: genDots(lenght, index),
        ),
      ),
    );
  }
}
