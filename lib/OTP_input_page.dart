import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:test_case_jun_mamonova/custom_widgets/custom_text_widget.dart';
import 'dart:async';

import 'custom_widgets/custom_input_otp_widget.dart';
import 'custom_widgets/custom_svg_image_widget.dart';
import 'custom_widgets/custom_timer_widget.dart';

class OTPInputPage extends StatefulWidget {
  const OTPInputPage({super.key, required this.subList});
  final Map<String, dynamic> subList;

  @override
  State<OTPInputPage> createState() => _OTPInputPageState();
}

TextEditingController controller = TextEditingController();
final FocusNode focus = FocusNode();

final defaultPinTheme = PinTheme(
  width: 56,
  height: 58,
  textStyle: const TextStyle(
      color: Color(0xffA0A0A0),
      fontFamily: "Inter",
      fontWeight: FontWeight.w500,
      fontSize: 56),
  decoration: BoxDecoration(
    border: Border.all(color: Colors.transparent),
    // borderRadius: BorderRadius.circular(20),
  ),
);

final focusedPinTheme = defaultPinTheme.copyDecorationWith(
  border: Border.all(color: Colors.transparent),
  // borderRadius: BorderRadius.circular(8),
);

final submittedPinTheme = defaultPinTheme.copyWith(
  decoration: defaultPinTheme.decoration?.copyWith(
    color: Colors.transparent,
  ),
);

class _OTPInputPageState extends State<OTPInputPage> {
  late Timer _timer;
  bool _flag = true;
  int _start = 0;
  String valueOfField = "0000";

  void startTimer() {
    _start = widget.subList["3414"]["timerDuration"];
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
            _flag = false;
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  void onFocusChange() {
    if (valueOfField == "0000" && focus.hasFocus) {
      // print("Нажали на контроллер");
      controller.text = "";
    } else if (valueOfField != "0000") {
      // print("ушли с него");
      controller.text = valueOfField;
    } else {
      // print("почему то елсе");
      controller.text = "0000";
    }
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    startTimer();
    controller.text = valueOfField;
    focus.addListener(onFocusChange);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 55,
        leadingWidth: 70,
        leading: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 8),
          child: Container(
            alignment: Alignment.center,
            decoration: const BoxDecoration(
              color: Color(0xffF5F6FA),
              borderRadius: BorderRadius.all(Radius.circular(30)),
            ),
            child: IconButton(
              icon: const Icon(
                Icons.arrow_back,
                color: Color(0xff0F161E),
                // size: 16,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Image.asset(
          'assets/logo.png',
          height: 32,
        ),
      ),
      body: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.all(14.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.purple.withOpacity(0.3),
                          spreadRadius: 0,
                          blurRadius: 30,
                          offset: const Offset(0, 5),
                        ),
                      ],
                    ),
                    child: CustomSvgImageWidget(
                      listik: widget.subList["63245"],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.all(16.0),
                child: CustomTextWidget(
                  listik: widget.subList['76345'],
                  styles: const TextStyle(
                      color: Color(0xff202230),
                      fontFamily: "Inter",
                      fontWeight: FontWeight.w600,
                      fontSize: 24),
                )),
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: CustomInputOTPWidget(
                  listik: widget.subList["535"],
                  focus: focus,
                  controller: controller,
                  onFocusChange: onFocusChange,
                  valueOfField: valueOfField,
                  changeValue: (String value) {
                    setState(() {
                      valueOfField = value;
                    });
                  },
                )),
            CustomTimerWidget(
              listik: widget.subList["3414"],
              flag: _flag,
              start: _start,
              startTimer: startTimer,
              setTimer: (bool value) {
                setState(() {
                  _flag = value;
                });
              },
            )

            // Text(timer);
          ],
        ),
      ),
    );
  }
}
