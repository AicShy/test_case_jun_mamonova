import 'package:flutter/material.dart';
import 'models.dart';
import 'authorization_screen.dart';
import 'OTP_input_page.dart';

Map<String, dynamic> mainList = {};

class Parser {
  Widget buildWidget(SimpleWidget widget) {
    if (widget is MainNodeWidget) {
      if (widget.name == "authotization") {
        makeList(widget.screen);
        return AuthorizationScreen(
          subList: mainList,
        );
      } else if (widget.name == "otpCode") {
        makeList(widget.screen);
        return OTPInputPage(subList: mainList);
      }
    }
    return ErrorWidget("exception");
  }

  Map<String, dynamic> makeList(SimpleWidget widget) {
    if (widget is ScreenWidget) {
      // listik.add({"fields": widget.fields});
      // widget.fields.map((e) => makeList(e));
      for (var element in widget.fields) {
        makeList(element);
      }
    } else if (widget is TextWidget) {
      // listik[widget.id] = [];
      mainList[widget.id.toString()] = {
        "text": widget.text,
        "textAlign": widget.textAlign,
        "visibility": widget.visibility
      };
    } else if (widget is TextFieldWidget) {
      mainList[widget.id.toString()] = {
        "visibility": widget.visibility,
        "security": widget.security,
        "backgroundColor": widget.backgroundColor,
        "borderColor": widget.borderColor,
        "errorBorderColor": widget.errorBorderColor,
        "mask": widget.mask,
        "replacementSymbol": widget.replacementSymbol,
        "label": widget.label,
        "text": widget.text,
        "placeholder": widget.placeholder,
        "errorText": widget.errorText,
      };
    } else if (widget is SliderWidget) {
      mainList[widget.id.toString()] = {
        "visibility": widget.visibility,
        "backgroundColor": widget.backgroundColor,
        "colorСircle": widget.colorCircle,
        "activeColorСircle": widget.activeColorCircle,
        "cards": widget.cards,
      };
    } else if (widget is ButtonWidget) {
      mainList[widget.id.toString()] = {
        "textAlign": widget.textAlign,
        "visibility": widget.visibility,
        "backgroundColor": widget.backgroundColor,
        "textColor": widget.textColor,
        "borderColor": widget.backgroundColor,
        "text": widget.text
      };
    } else if (widget is ImageWidget) {
      mainList[widget.id.toString()] = {
        "visibility": widget.visibility,
        "imageUrl": widget.imageUrl
      };
    } else if (widget is InputOTPWidget) {
      mainList[widget.id.toString()] = {
        "visibility": widget.visibility,
        "numberSymbols": widget.numberSymbols,
        "pointColor": widget.pointColor,
        "borderColor": widget.borderColor,
      };
    } else if (widget is TimerWidget) {
      mainList[widget.id.toString()] = {
        "visibility": widget.visibility,
        "textAlign": widget.textAlign,
        "loaderColor": widget.loaderColor,
        "timerDuration": widget.timerDuration,
        "waitingText": widget.waitingText,
        "endText": widget.endText,
        "url": widget.url
      };
    }
    return mainList;
  }

  SimpleWidget configParser(Map<String, dynamic> json) {
    // createList();
    if (json['name'] != null && json['screen'] != null) {
      // print("Мы ноду обрабатываем");
      return MainNodeWidget(
          name: json['name'] ?? "", screen: configParser(json['screen']));
    } else if (json['fields'] != null) {
      // print("Мы скрин обрабатываем");
      return ScreenWidget(
          id: json['id'] ?? 0,
          fields: (json["fields"] as List<dynamic>)
              .map((e) => configParser(e))
              .toList());
    } else if (json["fieldType"] != null && json["fieldType"] == 'text') {
      // print("Мы текст обрабатываем");
      return TextWidget(
          id: json["id"] ?? 0,
          text: json["properties"]["text"] ?? "",
          textAlign: json["properties"]["textAlign"] ?? "center",
          visibility: json["properties"]["visibility"] ?? true);
    } else if (json["fieldType"] != null && json["fieldType"] == 'textField') {
      // print("Мы текст обрабатываем");
      return TextFieldWidget(
          id: json['id'] ?? 0,
          visibility: json["properties"]['visibility'] ?? true,
          security: json["properties"]['security'] ?? true,
          backgroundColor: json["properties"]['backgroundColor'] ?? "#FFFFFF",
          borderColor: json["properties"]['borderColor'] ?? "#FFFFFF",
          errorBorderColor: json["properties"]['errorBorderColor'] ?? "#FFFFFF",
          mask: json["properties"]['mask'] ?? "+91 ___ ____ __ __",
          replacementSymbol: json["properties"]['replacementSymbol'] ?? "*",
          label: json["properties"]['label'] ?? "",
          text: json["properties"]['text'] ?? "",
          placeholder: json["properties"]['placeholder'] ?? "",
          errorText: json["properties"]['errorText'] ?? "");
    } else if (json['fieldType'] != null && json["fieldType"] == 'slider') {
      // print("Мы СЛАЙДЕР обрабатываем");
      return SliderWidget(
        id: json['id'] ?? 0,
        visibility: json["properties"]["visibility"] ?? true,
        backgroundColor: json["properties"]["backgroundColor"] ?? "#FFFFFF",
        colorCircle: json["properties"]["colorСircle"] ?? "#FFFFFF",
        activeColorCircle: json["properties"]["activeColorСircle"] ?? "#FFFFFF",
        cards: json['properties']["cards"],
      );
    } else if (json["fieldType"] != null && json["fieldType"] == "button") {
      // print("Мы BUTTON обрабатываем");
      return ButtonWidget(
        id: json['id'] ?? 0,
        visibility: json["properties"]["visibility"] ?? true,
        textAlign: json["properties"]["textAlign"] ?? "center",
        backgroundColor: json["properties"]["backgroundColor"] ?? "#FFFFFF",
        textColor: json["properties"]["textColor"] ?? "#FFFFFF",
        borderColor: json["properties"]["borderColor"] ?? "#FFFFFF",
        text: json["properties"]["text"] ?? "",
      );
    } else if (json["fieldType"] != null && json["fieldType"] == "image") {
      return ImageWidget(
          id: json['id'] ?? 0,
          visibility: json["properties"]["visibility"] ?? true,
          imageUrl: json["properties"]["imageUrl"] ??
              "https://storage.googleapis.com/cms-storage-bucket/ec64036b4eacc9f3fd73.svg");
    } else if (json["fieldType"] != null && json["fieldType"] == "inputOTP") {
      return InputOTPWidget(
          id: json['id'] ?? 0,
          visibility: json["properties"]["visibility"] ?? true,
          numberSymbols: json["properties"]["numberSymbols"] ?? 4,
          pointColor: json["properties"]["pointColor"] ?? "#FFFFFF",
          borderColor: json["properties"]["borderColor"] ?? "#FFFFFF");
    } else if (json["fieldType"] != null && json["fieldType"] == "timer") {
      return TimerWidget(
        id: json['id'] ?? 0,
        visibility: json["properties"]["visibility"] ?? true,
        textAlign: json["properties"]['textAlign'] ?? "center",
        loaderColor: json["properties"]["loaderColor"] ?? "#FFFFFF",
        timerDuration: json["properties"]["timerDuration"] ?? 60,
        waitingText: json["properties"]["waitingText"] ?? "",
        endText: json['properties']["endText"] ?? "",
        url: json["properties"]["url"] ?? "",
      );
    }
    return ErrorCustomWidget();
  }
}
